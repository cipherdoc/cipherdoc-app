import { teal, tealDark } from '@tamagui/colors';
import { Variable, createTokens } from '@tamagui/web';
import {
  MaskOptions,
  addChildren,
  applyMask,
  createStrengthenMask,
  createTheme,
  createWeakenMask,
  skipMask,
} from '@tamagui/create-theme';

export const size = {
  $0: 0,
  '$0.25': 2,
  '$0.5': 4,
  '$0.75': 8,
  $1: 20,
  '$1.5': 24,
  $2: 28,
  '$2.5': 32,
  $3: 36,
  '$3.5': 40,
  $4: 44,
  $true: 44,
  '$4.5': 48,
  $5: 52,
  $6: 64,
  $7: 74,
  $8: 84,
  $9: 94,
  $10: 104,
  $11: 124,
  $12: 144,
  $13: 164,
  $14: 184,
  $15: 204,
  $16: 224,
  $17: 224,
  $18: 244,
  $19: 264,
  $20: 284,
};

type SizeKeysIn = keyof typeof size;
type Sizes = {
  [Key in SizeKeysIn extends `$${infer Key}` ? Key : SizeKeysIn]: number;
};
type SizeKeys = `${keyof Sizes extends `${infer K}` ? K : never}`;

const spaces = Object.entries(size).map(([k, v]) => {
  return [k, sizeToSpace(v)] as const;
});

// a bit odd but keeping backward compat for values >8 while fixing below
function sizeToSpace(v: number) {
  if (v === 0) return 0;
  if (v === 2) return 0.5;
  if (v === 4) return 1;
  if (v === 8) return 1.5;
  if (v <= 16) return Math.round(v * 0.333);
  return Math.floor(v * 0.7 - 12);
}

const spacesNegative = spaces.slice(1).map(([k, v]) => [`-${k.slice(1)}`, -v]);

type SizeKeysWithNegatives =
  | Exclude<`-${SizeKeys extends `$${infer Key}` ? Key : SizeKeys}`, '-0'>
  | SizeKeys;

export const space: {
  [Key in SizeKeysWithNegatives]: Key extends keyof Sizes ? Sizes[Key] : number;
} = {
  ...Object.fromEntries(spaces),
  ...Object.fromEntries(spacesNegative),
} as any;

export const zIndex = {
  0: 0,
  1: 100,
  2: 200,
  3: 300,
  4: 400,
  5: 500,
};

export const colorTokens = {
  light: {
    teal: teal,
  },
  dark: {
    teal: tealDark,
  },
};

export const darkColors = {
  ...colorTokens.dark.teal,
};

export const lightColors = {
  ...colorTokens.light.teal,
};

export const color = {
  ...postfixObjKeys(lightColors, 'Light'),
  ...postfixObjKeys(darkColors, 'Dark'),
};

function postfixObjKeys<A extends { [key: string]: Variable<string> | string }, B extends string>(
  obj: A,
  postfix: B
): {
  [Key in `${keyof A extends string ? keyof A : never}${B}`]: Variable<string> | string;
} {
  return Object.fromEntries(Object.entries(obj).map(([k, v]) => [`${k}${postfix}`, v])) as any;
}

export const radius = {
  0: 0,
  1: 3,
  2: 5,
  3: 7,
  4: 9,
  true: 9,
  5: 10,
  6: 16,
  7: 19,
  8: 22,
  9: 26,
  10: 34,
  11: 42,
  12: 50,
};

export const tokens = createTokens({
  color,
  radius,
  zIndex,
  space,
  size,
});

type ColorName = keyof typeof colorTokens.dark;

const lightTransparent = 'rgba(255,255,255,0)';
const darkTransparent = 'rgba(10,10,10,0)';

// background => foreground
const palettes = {
  dark: [
    darkTransparent,
    '#050505',
    '#151515',
    '#191919',
    '#232323',
    '#282828',
    '#323232',
    '#424242',
    '#494949',
    '#545454',
    '#626262',
    '#a5a5a5',
    '#fff',
    lightTransparent,
  ],
  light: [
    lightTransparent,
    '#fff',
    '#f9f9f9',
    'hsl(0, 0%, 97.3%)',
    'hsl(0, 0%, 95.1%)',
    'hsl(0, 0%, 94.0%)',
    'hsl(0, 0%, 92.0%)',
    'hsl(0, 0%, 89.5%)',
    'hsl(0, 0%, 81.0%)',
    'hsl(0, 0%, 56.1%)',
    'hsl(0, 0%, 50.3%)',
    'hsl(0, 0%, 42.5%)',
    'hsl(0, 0%, 9.0%)',
    darkTransparent,
  ],
};

const lightShadowColor = 'rgba(0,0,0,0.02)';
const lightShadowColorStrong = 'rgba(0,0,0,0.066)';
const darkShadowColor = 'rgba(0,0,0,0.2)';
const darkShadowColorStrong = 'rgba(0,0,0,0.3)';

const templateColors = {
  color1: 1,
  color2: 2,
  color3: 3,
  color4: 4,
  color5: 5,
  color6: 6,
  color7: 7,
  color8: 8,
  color9: 9,
  color10: 10,
  color11: 11,
  color12: 12,
};

const templateShadows = {
  shadowColor: 1,
  shadowColorHover: 1,
  shadowColorPress: 2,
  shadowColorFocus: 2,
};

const toSkip = {
  ...templateShadows,
};

const override = Object.fromEntries(Object.entries(toSkip).map(([k]) => [k, 0]));
const overrideShadows = Object.fromEntries(Object.entries(templateShadows).map(([k]) => [k, 0]));
const overrideWithColors = {
  ...override,
  color: 0,
  colorHover: 0,
  colorFocus: 0,
  colorPress: 0,
};

// templates use the palette and specify index
// negative goes backwards from end so -1 is the last item
const template = {
  ...templateColors,
  ...toSkip,
  background: 2,
  backgroundHover: 3,
  backgroundPress: 4,
  backgroundFocus: 2,
  backgroundStrong: 1,
  backgroundTransparent: 0,
  color: -1,
  colorHover: -2,
  colorPress: -1,
  colorFocus: -2,
  colorTransparent: -0,
  borderColor: 4,
  borderColorHover: 5,
  borderColorPress: 3,
  borderColorFocus: 4,
  placeholderColor: -4,
};

const lightShadows = {
  shadowColor: lightShadowColorStrong,
  shadowColorHover: lightShadowColorStrong,
  shadowColorPress: lightShadowColor,
  shadowColorFocus: lightShadowColor,
};

const darkShadows = {
  shadowColor: darkShadowColorStrong,
  shadowColorHover: darkShadowColorStrong,
  shadowColorPress: darkShadowColor,
  shadowColorFocus: darkShadowColor,
};

const lightTemplate = {
  ...template,
  ...lightShadows,
};

const darkTemplate = { ...template, ...darkShadows };

const light = createTheme(palettes.light, lightTemplate);
const dark = createTheme(palettes.dark, darkTemplate);

type SubTheme = typeof light;

const baseThemes: {
  light: SubTheme;
  dark: SubTheme;
} = {
  light,
  dark,
};

const masks = {
  skip: skipMask,
  weaker: createWeakenMask(),
  stronger: createStrengthenMask(),
};

// default mask options for most uses
const maskOptions: MaskOptions = {
  override,
  skip: toSkip,
  // avoids the transparent ends
  max: palettes.light.length - 2,
  min: 1,
};

const transparent = (hsl: string, opacity = 0) =>
  hsl.replace(`%)`, `%, ${opacity})`).replace(`hsl(`, `hsla(`);

// setup colorThemes and their inverses
const [lightColorThemes, darkColorThemes] = [colorTokens.light, colorTokens.dark].map(
  (colorSet, i) => {
    const isLight = i === 0;
    const theme = baseThemes[isLight ? 'light' : 'dark'];

    return Object.fromEntries(
      Object.keys(colorSet).map((color) => {
        const colorPalette = Object.values(colorSet[color]) as string[];
        // were re-ordering these
        const [head, tail] = [
          colorPalette.slice(0, 6),
          colorPalette.slice(colorPalette.length - 5),
        ];
        // add our transparent colors first/last
        // and make sure the last (foreground) color is white/black rather than colorful
        // this is mostly for consistency with the older theme-base
        const palette = [
          transparent(colorPalette[0]),
          ...head,
          ...tail,
          theme.color,
          transparent(colorPalette[colorPalette.length - 1]),
        ];

        const colorTheme = createTheme(
          palette,
          isLight
            ? {
                ...lightTemplate,
                // light color themes are a bit less sensitive
                borderColor: 4,
                borderColorHover: 5,
                borderColorFocus: 4,
                borderColorPress: 4,
              }
            : darkTemplate
        );

        return [color, colorTheme];
      })
    ) as Record<ColorName, SubTheme>;
  }
);

const allThemes = addChildren(baseThemes, (name, theme) => {
  const isLight = name === 'light';
  const inverseName = isLight ? 'dark' : 'light';
  const inverseTheme = baseThemes[inverseName];
  const colorThemes = isLight ? lightColorThemes : darkColorThemes;
  const inverseColorThemes = isLight ? darkColorThemes : lightColorThemes;

  const allColorThemes = addChildren(colorThemes, (colorName, colorTheme) => {
    const inverse = inverseColorThemes[colorName];
    return {
      ...getAltThemes({
        theme: colorTheme,
        inverse,
        isLight,
      }),
      ...getComponentThemes(colorTheme, inverse, isLight),
    };
  });

  const baseSubThemes = {
    ...getAltThemes({ theme, inverse: inverseTheme, isLight }),
    ...getComponentThemes(theme, inverseTheme, isLight),
  };

  return {
    ...baseSubThemes,
    ...allColorThemes,
  };
});

function getAltThemes({
  theme,
  inverse,
  isLight,
  activeTheme,
}: {
  theme: SubTheme;
  inverse: SubTheme;
  isLight: boolean;
  activeTheme?: SubTheme;
}) {
  const maskOptionsAlt = {
    ...maskOptions,
    override: overrideShadows,
  };
  const alt1 = applyMask(theme, masks.weaker, maskOptionsAlt);
  const alt2 = applyMask(alt1, masks.weaker, maskOptionsAlt);

  const active =
    activeTheme ??
    (process.env.ACTIVE_THEME_INVERSE
      ? inverse
      : (() => {
          return applyMask(theme, masks.weaker, {
            ...maskOptions,
            strength: 3,
            skip: {
              ...maskOptions.skip,
              color: 1,
            },
          });
        })());

  return addChildren({ alt1, alt2, active }, (_, subTheme) => {
    return getComponentThemes(subTheme, subTheme === inverse ? theme : inverse, isLight);
  });
}

function getComponentThemes(theme: SubTheme, inverse: SubTheme, isLight: boolean) {
  const componentMaskOptions: MaskOptions = {
    ...maskOptions,
    override: overrideWithColors,
    skip: {
      ...maskOptions.skip,
      // skip colors too just for component sub themes
      ...templateColors,
    },
  };
  const weaker1 = applyMask(theme, masks.weaker, componentMaskOptions);
  const base = applyMask(weaker1, masks.stronger, componentMaskOptions);
  const weaker2 = applyMask(weaker1, masks.weaker, {
    ...componentMaskOptions,
    override: overrideWithColors,
  });
  const stronger1 = applyMask(theme, masks.stronger, componentMaskOptions);
  const inverse1 = applyMask(inverse, masks.weaker, componentMaskOptions);
  const inverse2 = applyMask(inverse1, masks.weaker, componentMaskOptions);
  const strongerBorderLighterBackground: SubTheme = isLight
    ? {
        ...stronger1,
        borderColor: weaker1.borderColor,
        borderColorHover: weaker1.borderColorHover,
        borderColorPress: weaker1.borderColorPress,
        borderColorFocus: weaker1.borderColorFocus,
      }
    : {
        ...applyMask(theme, masks.skip, componentMaskOptions),
        borderColor: weaker1.borderColor,
        borderColorHover: weaker1.borderColorHover,
        borderColorPress: weaker1.borderColorPress,
        borderColorFocus: weaker1.borderColorFocus,
      };

  const overlayTheme = {
    background: isLight ? 'rgba(0,0,0,0.5)' : 'rgba(0,0,0,0.9)',
  } as SubTheme;

  const weaker1WithoutBorder = {
    ...weaker1,
    borderColor: 'transparent',
    borderColorHover: 'transparent',
  };

  const weaker2WithoutBorder = {
    ...weaker2,
    borderColor: 'transparent',
    borderColorHover: 'transparent',
  };

  return {
    ListItem: isLight ? stronger1 : base,
    Card: weaker1,
    Button: weaker2WithoutBorder,
    Checkbox: weaker2,
    DrawerFrame: weaker1,
    SliderTrack: stronger1,
    SliderTrackActive: weaker2,
    SliderThumb: inverse1,
    Progress: weaker1,
    ProgressIndicator: inverse,
    Switch: weaker2,
    SwitchThumb: inverse2,
    TooltipArrow: weaker1,
    TooltipContent: weaker2,
    Input: strongerBorderLighterBackground,
    TextArea: strongerBorderLighterBackground,
    Tooltip: inverse1,
    // make overlays always dark
    SheetOverlay: overlayTheme,
    DialogOverlay: overlayTheme,
    ModalOverlay: overlayTheme,
  };
}

export const themes = {
  ...allThemes,
  // bring back the full type, the rest use a subset to avoid clogging up ts,
  // tamagui will be smart and use the top level themes as the type for useTheme() etc
  light: createTheme(palettes.light, lightTemplate, { nonInheritedValues: lightColors }),
  dark: createTheme(palettes.dark, darkTemplate, { nonInheritedValues: darkColors }),
};

// if (process.env.NODE_ENV === 'development') {
//   console.log(JSON.stringify(themes).length)
// }
