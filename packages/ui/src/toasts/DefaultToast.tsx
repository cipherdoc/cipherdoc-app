import { Toast, useToastState } from '@tamagui/toast';
import { Unlock as UnlockIcon } from '@tamagui/lucide-icons';
import { XStack } from 'tamagui';
import React from 'react';

const icons = {
  unlock: UnlockIcon,
};

const DefaultToast = () => {
  const toast = useToastState();

  if (!toast || toast.isHandledNatively) {
    return null;
  }

  const icon = toast.notificationOptions?.icon && icons[toast.notificationOptions?.icon];

  return (
    <Toast
      key={toast.id}
      duration={toast.duration ?? 3000}
      y={0}
      animation="quick"
      enterStyle={{ y: -200 }}
      exitStyle={{ y: -200 }}
      type="foreground"
      theme="teal"
      backgroundColor="$color5"
    >
      <XStack my="$2" justifyContent='center' alignItems='center'>
        {React.createElement(icon, { size: '$1' })}
        <Toast.Title fontSize="$3" ml="$3">
          {toast.title}
        </Toast.Title>
      </XStack>
    </Toast>
  );
};

export default DefaultToast;
