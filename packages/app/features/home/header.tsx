import { XStack } from '@my/ui';
import React from 'react';
import {
  User as UserIcon,
  HelpCircle as HelpCircleIcon,
  LogOut as LogOutIcon,
} from '@tamagui/lucide-icons';

const Header = () => {
  return (
    <XStack jc="space-between">
      <UserIcon />
      <XStack space="$3.5">
        <HelpCircleIcon />
        <LogOutIcon />
      </XStack>
    </XStack>
  );
};

export default Header;
