import React from 'react';
import DocumentCard, { DocumentCardTypes } from './document';
import { Circle, XStack, Text, Stack } from '@my/ui';

const Carousel = ({ documents, onDocumentChange }: SliderTypes.Props) => {
  return (
    <XStack fw='nowrap' alignSelf='flex-start' ml="$5" space='$5'>
      {documents.map((document, i) => (
        <Stack key={`Slider__DocumentCard__${i}`} fg={0} fs={0} fb='auto'>
            <DocumentCard  {...document} />
        </Stack>
      ))}
    </XStack>
  );
};

Carousel.Dots = ({ active, count }: SliderTypes.Dots.Props) => {
  return (
    <XStack space={10}>
      {[...Array(count)].map((_, i) => (
        <Carousel.Dot key={`Slider.Dot__${i}`} isActive={active == i} />
      ))}
    </XStack>
  );
};

Carousel.Dot = ({ isActive }: SliderTypes.Dot.Props) => {
  return <Circle bg={isActive ? '$color9' : '$color6'} size={isActive ? 10 : '$0.75'} />;
};

export default Carousel;

export namespace SliderTypes {
  export type Props = {
    documents: DocumentCardTypes.Props[];

    onDocumentChange?: (index: number) => void;
  };

  export namespace Dot {
    export type Props = {
      isActive?: boolean;
    };
  }

  export namespace Dots {
    export type Props = {
      count: number;
      active: number;
    };
  }
}
