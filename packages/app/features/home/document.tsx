import { Image, Text, YStack } from '@my/ui';
import React from 'react';
import lock from './assets/lock.png';
import Document from '../common/documents';

import id from './assets/id.png';

const images: { [T in Document.Type]: typeof id } = {
  id,
};

const DocumentCard = ({ type, isLocked = true }: DocumentCardTypes.Props) => {
  const image = images[type];
  const title = Document.idNameMap[type];

  return (
    <YStack ai="center" pos="relative">
      {isLocked && <Image source={lock} pos="absolute" zi={2} t="$-5" r="$-2" />}
      <Image source={image} />
      <Text fontSize={16} mt="$6">
        {title}
      </Text>
    </YStack>
  );
};

export default DocumentCard;

export namespace DocumentCardTypes {
  export type Props = {
    type: Document.Type;
    isLocked?: boolean;
  };
}
