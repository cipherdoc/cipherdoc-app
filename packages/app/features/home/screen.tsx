import { YStack, Image, XStack, Button } from '@my/ui';
import React, {  useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

import bg from './assets/bg.png';
import Header from './header';
import { DocumentCardTypes } from './document';
import { Share as ShareIcon, Eye as EyeIcon } from '@tamagui/lucide-icons';
import Carousel from './carousel';
import { useLink } from 'solito/link';
import { useShareStore } from 'app/provider/store/share';

const documents: DocumentCardTypes.Props[] = [
  { type: 'id', isLocked: true },
  { type: 'id', isLocked: false },
];

const HomeScreen = () => {
  const [selectedDocument, setSelectedDocument] = useState(0);

  const setSharingDocument = useShareStore((s) => s.setDocument);

  const shareLink = useLink({
    href: '/share',
  });

  const onSharePress = () => {
    setSharingDocument({
      type: documents[selectedDocument]!.type,
      id: 'abc',
      encryptedKey: {
        nonceB64: 'Rlfh6veZUBJPPRB4R9Iny7Xj1SXqUrqj',
        valueB64:
          'Q9RPQ6p4q5byJJbDucsnLzPhlZH0CcZF/Q1CiMiJUjjJvWZpClwh95Kdlcd9fT4cLUkex3My76aLlu596/vSnUK46ZKWjXz05hn6eYeHDD0=',
      },
    });
    shareLink.onPress();
  };

  return (
    <>
      <Image source={bg} h="100%" w="100%" pos="absolute" zIndex={0} />
      <SafeAreaView>
        <YStack mt="$5" mx="$5">
          <Header />
          <YStack mx="$2" mt="$13" ai="center">
            <Carousel documents={documents} onDocumentChange={setSelectedDocument} />
            <XStack mt="$10" mb="$7" jc="space-between" w="100%">
              <Button icon={ShareIcon} size="$6" {...shareLink} onPress={onSharePress}>
                Sdílet
              </Button>
              <Button icon={EyeIcon} size="$6">
                Ukázat
              </Button>
            </XStack>
            <Carousel.Dots count={documents.length} active={selectedDocument} />
          </YStack>
        </YStack>
      </SafeAreaView>
    </>
  );
};

export default HomeScreen;
