namespace Document {
  export type Type = 'id';

  export const idNameMap: { [T in Type]: string } = {
    id: 'Občanský průkaz',
  };
}

export default Document;
