import { Input, InputProps, Spinner, Stack, XStack } from '@my/ui';
import { AlertTriangle } from '@tamagui/lucide-icons';
import React from 'react';

const InputWithStatus = ({ status, ...rest }: InputWithStatusTypes.Props) => {
  return (
    <XStack pos="relative">
      <Input {...rest} />
      {status === 'processing' && <Spinner color="$color9" pos="absolute" right="$4" top="$3" />}
      {status === 'fail' && (
        <Stack right="$7" top="$3">
          <AlertTriangle size="$1" />
        </Stack>
      )}
    </XStack>
  );
};

export default InputWithStatus;

export namespace InputWithStatusTypes {
  export type Props = InputProps & {
    status: 'idle' | 'processing' | 'fail' | 'success';
  };
}
