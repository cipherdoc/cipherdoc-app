import { Image, Text, XStack, XStackProps, YStack, YStackProps } from '@my/ui';
import { AlertCircle as AlertCircleIcon } from '@tamagui/lucide-icons';
import React, { ComponentProps } from 'react';

const BigInfo = ({ image, imageSize, text, children, ...containerProps }: BigInfoTypes.Props) => {
  return (
    <YStack {...containerProps}>
      <BigInfo.Head text={text} />
      <BigInfo.Body image={image} imageSize={imageSize}>{children}</BigInfo.Body>
    </YStack>
  );
};

BigInfo.Head = ({ text }: BigInfoTypes.Head.Props) => (
  <XStack>
    <AlertCircleIcon />
    <Text fontSize="$5" fontWeight="500" ml="$2.5">
      {text}
    </Text>
  </XStack>
);

BigInfo.Body = ({
  image,
  imageSize = '$11',
  children,
  ...containerProps
}: BigInfoTypes.Body.Props) => (
  <XStack ai="center" mt="$-7" {...containerProps}>
    <Image w={imageSize} source={image} resizeMode="contain" mr='$2' />
    {children}
  </XStack>
);

export default BigInfo;

export namespace BigInfoTypes {
  export type Props = Head.Props & Body.Props & YStackProps;

  export namespace Head {
    export type Props = {
      text: string;
    };
  }

  export namespace Body {
    export type Props = {
      image: ComponentProps<typeof Image>['source'];
      imageSize?: ComponentProps<typeof Image>['w'];
    } & XStackProps;
  }
}
