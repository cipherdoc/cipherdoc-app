namespace Client {
    export type Type = {
        id: string;
        nickname: string;
        name: string;
    }
}

export default Client;
