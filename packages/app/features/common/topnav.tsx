import React from 'react';
import { XStack } from '@my/ui';
import { ArrowLeft as ArrowLeftIcon, HelpCircle as HelpCircleIcon } from '@tamagui/lucide-icons';
import { useRouter } from 'solito/router';

const TopNav = () => {
  const { back } = useRouter();

  return (
    <XStack jc="space-between">
      <ArrowLeftIcon onPress={back} />
      <HelpCircleIcon />
    </XStack>
  );
};

export default TopNav;

export namespace TopNavTypes {}
