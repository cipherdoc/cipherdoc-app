import { XStack, Text } from '@my/ui';
import { IconProps } from '@tamagui/lucide-icons/types/IconProps';
import React from 'react';

const Header = ({ main, sub }: HeaderTypes.Props) => {
  return (
    <>
      <Header.Main {...main} />
      {sub && <Header.Sub {...sub} />}
    </>
  );
};

Header.Main = ({ text, icon }: HeaderTypes.Main.Props) => {
  return (
    <XStack ai="center">
      {icon && React.createElement(icon, { size: '$2.5' })}
      <Text fontFamily="$heading" fontSize="$8" ml={icon && '$2.5'}>
        {text}
      </Text>
    </XStack>
  );
};

Header.Sub = ({ topic, text }: HeaderTypes.Sub.Props) => {
  return (
    <XStack mt="$1.5">
      <Text fontFamily="$body" fontWeight="500">
        {topic}
        {text && ': '}
      </Text>
      {text && <Text>{text}</Text>}
    </XStack>
  );
};

export default Header;

export namespace HeaderTypes {
  export type Props = {
    main: Main.Props;
    sub?: Sub.Props;
  };

  export namespace Main {
    export type Props = {
      icon?: React.FC<IconProps>;
      text: string;
    };
  }

  export namespace Sub {
    export type Props = {
      topic: string;
      text?: string;
    };
  }
}
