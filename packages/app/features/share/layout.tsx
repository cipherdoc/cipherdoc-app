import { Button, Spinner, Stack, YStack } from '@my/ui';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import TopNav from '../common/topnav';
import Header from './header';
import Sharing from './constants';
import Document from '../common/documents';
import { ArrowRight as ArrowRightIcon } from '@tamagui/lucide-icons';
import { IconProps } from '@tamagui/lucide-icons/types/IconProps';

const Layout = ({ children, step, document, next, isLoading = false }: LayoutTypes.Props) => {
  const { isDisabled = false } = next;

  let icon = !isDisabled
    ? next.icon
      ? React.createElement(next.icon)
      : ArrowRightIcon
    : undefined;
  if (!isDisabled && isLoading) {
    icon = <Spinner />;
  }

  return (
    <SafeAreaView>
      <YStack mt="$5" mx="$5" pos="relative" h="100%">
        <Stack mb="$6" space="$3.5">
          <TopNav />
          <Header {...{ step, document }} />
        </Stack>
        {children}
        <Button
          {...next.link}
          pos="absolute"
          bottom={100}
          left={0}
          iconAfter={icon}
          right={0}
          disabled={isDisabled}
        >
          {next.text ?? 'Pokračovat'}
        </Button>
      </YStack>
    </SafeAreaView>
  );
};

export default Layout;

export namespace LayoutTypes {
  export type Props = {
    children?: any | any[];
    step: Sharing.Step;
    document?: Document.Type;
    isLoading?: boolean;

    next: Next;
  };

  type Next = {
    link?: { href?: string; onPress?: () => void };
    isDisabled?: boolean;
    text?: string;
    icon?: React.FC<IconProps>;
  };
}
