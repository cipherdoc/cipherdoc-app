import React, { forwardRef, useState } from 'react';
import { Button, Image, Text, XGroup, YStack } from '@my/ui';
import bg from './assets/indexbg.png';
import Layout from './layout';
import Sharing from './constants';

import client from './assets/client.png';
import organization from './assets/organization.png';
import { useLink } from 'solito/link';
import { useShareStore } from 'app/provider/store/share';

const ShareScreen = () => {
  const document = useShareStore((s) => s.document?.type);
  const [type, setType] = useState<Sharing.Type>('client');
  const setShareType = useShareStore((s) => s.setType);

  const link = useLink({ href: `/share/${type}/1` });

  const onContinuePress = () => {
    setShareType(type);
    link.onPress();
  };

  const next = {
    link: { ...link, onPress: onContinuePress },
    text: type === 'client' ? 'Pokračovat' : 'Soon 👨‍💻',
    isDisabled: type !== 'client',
  };

  return (
    <>
      <Image source={bg} h="100%" w="100%" pos="absolute" zIndex={0} />
      <Layout document={document} next={next} step="index">
        <ShareScreen.ButtonGroup active={type} onChange={setType} />
        {type === 'client' ? <ShareScreen.Client /> : <ShareScreen.Organization />}
      </Layout>
    </>
  );
};

ShareScreen.Client = () => {
  return (
    <YStack ai="center" mt="$6">
      <Image source={client} width={280} height={220} resizeMode="contain" />
      <Text w="100%" textAlign="justify" fontSize="$6">
        Chráněné sdílení mezi uživateli je bezpečným způsobem pro potvrzení své digitalní identity
        jinému člověku.
      </Text>
    </YStack>
  );
};

ShareScreen.Organization = () => {
  return (
    <YStack ai="center" mt="$6">
      <Image source={organization} width={280} height={220} resizeMode="contain" />
      <Text w="100%" textAlign="justify" fontSize="$6">
        Chráněné sdílení je bezpečným způsobem pro organizaci ke zpracování vašich dokladů a
        potvrzení vaše identity.
      </Text>
    </YStack>
  );
};

ShareScreen.ButtonGroup = ({ active, onChange }: ShareScreenTypes.ButtonGroup.Props) => {
  return (
    <XGroup>
      <ShareScreen.ButtonGroup_Item
        type="client"
        isActive={active === 'client'}
        onPress={() => onChange?.('client')}
      />
      <ShareScreen.ButtonGroup_Item
        type="organization"
        isActive={active === 'organization'}
        onPress={() => onChange?.('organization')}
      />
    </XGroup>
  );
};

ShareScreen.ButtonGroup_Item = forwardRef(
  ({ type, isActive, onPress }: ShareScreenTypes.ButtonGroup.Button.Props, ref: any) => {
    const text = type === 'client' ? 'Uživatel' : 'Organizace';
    const active = isActive && ({ theme: 'alt2', color: '$0' } as const);

    return (
      <XGroup.Item>
        <Button w="50%" size="$3" fontFamily="$body" fontWeight="500" onPress={onPress} {...active}>
          {text}
        </Button>
      </XGroup.Item>
    );
  }
);

export default ShareScreen;

namespace ShareScreenTypes {
  export namespace ButtonGroup {
    export type Props = {
      active: Sharing.Type;
      onChange: (type: Sharing.Type) => void;
    };

    export namespace Button {
      export type Props = {
        type: Sharing.Type;
        isActive?: boolean;
        onPress?: () => void;
      };
    }
  }
}
