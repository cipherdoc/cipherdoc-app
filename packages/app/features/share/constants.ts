namespace Sharing {
  export type Type = 'client' | 'organization';

  export const topic = 'Sdílení';

  type Digit = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

  export type Step = `${Type}-${Digit}` | 'index';

  export const stepTitleMap: { [T in Step]?: string } = {
    index: 'Zvolte typ adresáta',
    'client-1': 'Vyhledejte uživatele',
    'client-2': 'Nastavte dobu povolení',
    'client-3': 'Rekapitulace',
  };
}

export default Sharing;
