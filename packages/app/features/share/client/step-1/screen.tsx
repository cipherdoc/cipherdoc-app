import React, { useState } from 'react';
import { Image, Text } from '@my/ui';
import Layout from '../../layout';
import { useLink } from 'solito/link';

import bg from './assets/bg.png';
import alert from './assets/alert.png';
import useClientSearch from './useClientSearch';
import InputWithStatus from 'app/features/common/input';
import ClientCards from './users';
import BigInfo from 'app/features/common/biginfo';
import { useClientShareStore } from 'app/provider/store/share';

const ShareClientStep1Screen = () => {
  const document = useClientShareStore((s) => s.document?.type);
  const setTargetUser = useClientShareStore((s) => s.setTargetUser);
  const { clients, status, onNicknameChanged } = useClientSearch();
  const [selectedId, setSelectedId] = useState<string>();

  const clientsToDisplay = !selectedId
    ? clients
    : clients.filter((client) => client.id === selectedId);

  const link = useLink({ href: '/share/client/2' });

  const canContinue: boolean = !!selectedId;

  const onContinuePress = () => {
    setTargetUser(clientsToDisplay.find(c => c.id === selectedId));
    link.onPress();
  };

  const next = {
    link: { ...link, onPress: onContinuePress },
    isDisabled: !canContinue,
  };

  const onSelectedChange = (id: string) => {
    if (id === selectedId) {
      setSelectedId(undefined);
    } else {
      setSelectedId(id);
    }
  };

  return (
    <>
      <Image source={bg} h="100%" w="100%" pos="absolute" zIndex={0} />
      <Layout document={document} next={next} step="client-1">
        <InputWithStatus
          status={status}
          onChangeText={onNicknameChanged}
          placeholder="@nickname"
          w="100%"
        />
        <ClientCards
          selected={selectedId}
          onSelectedChange={onSelectedChange}
          clients={clientsToDisplay}
          mt="$5"
        />
        {canContinue && <ShareClientStep1Screen.Alert />}
      </Layout>
    </>
  );
};

ShareClientStep1Screen.Alert = () => {
  return (
    <BigInfo text="Dávejte pozor na data adresáta" image={alert} mt="$5">
      <Text flexShrink={1} mt="$-5" fontSize="$4" textAlign="justify">
        Každý uživatel mimo nickname má vlastní ID. Pokaždé pečlivě ho také zkontrolujte.{' '}
        <Text fontWeight="500">Adresáti mohou mít podobné nicknamy.</Text>
      </Text>
    </BigInfo>
  );
};

export default ShareClientStep1Screen;
