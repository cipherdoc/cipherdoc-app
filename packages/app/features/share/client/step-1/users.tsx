import { Avatar, XStack, YStack, Text, YStackProps } from '@my/ui';
import Client from 'app/features/common/clients';
import React, { useCallback } from 'react';
import { Keyboard } from 'react-native';

const ClientCards = ({ clients, selected, onSelectedChange, ...rest }: ClientCardsTypes.Props) => {
  return (
    <YStack space="$2" {...rest}>
      {clients.map((client, i) => (
        <ClientCards.Card
          key={`ClientCards__Card_${i}`}
          isSelected={selected === client.id}
          onPress={onSelectedChange}
          {...client}
        />
      ))}
    </YStack>
  );
};

ClientCards.Card = ({
  id,
  name,
  nickname,
  isSelected = false,
  onPress,
}: ClientCardsTypes.Card.Props) => {
  const avatarFallback = name
    .split(' ')
    .map((p) => p[0])
    .join('');

  const onContainerPress = useCallback(() => {
    Keyboard.dismiss();
    onPress?.(id);
  }, [onPress, id]);

  return (
    <XStack
      ai="center"
      px="$2.5"
      py="$3"
      bc={isSelected ? 'white' : '$color3'}
      br="$3"
      pressStyle={{ bc: '$color4' }}
      borderColor="$color5"
      borderStyle="solid"
      borderWidth={isSelected ? '$0.5' : 0}
      onPress={onContainerPress}
    >
      <Avatar circular>
        <Avatar.Fallback backgroundColor="$color6" ai="center" jc="center">
          <Text fontWeight="500">{avatarFallback}</Text>
        </Avatar.Fallback>
      </Avatar>
      <YStack ml="$3">
        <Text fontWeight="500">{name}</Text>
        <Text color="$color11">
          @{nickname} • #{id}
        </Text>
      </YStack>
    </XStack>
  );
};

export default ClientCards;

export namespace ClientCardsTypes {
  export type Props = YStackProps & {
    clients: Client.Type[];

    selected?: string;
    onSelectedChange?: (id: string) => void;
  };

  export namespace Card {
    export type Props = Client.Type & {
      isSelected?: boolean;
      onPress?: (id: string) => void;
    };
  }
}
