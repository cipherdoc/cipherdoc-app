import { debounce } from '@my/ui';
import Client from 'app/features/common/clients';
import { useCallback, useEffect, useReducer } from 'react';

const clients: Client.Type[] = [
  {
    id: 'bYdx',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx1',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx2',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx3',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx4',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYd5x',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx6',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
  {
    id: 'bYdx7',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },

  {
    id: 'bYdx9',
    name: 'Alex Goncharov',
    nickname: 'mrfullset',
  },
];

const useClientSearch = () => {
  const [state, dispatch] = useReducer(reducer, defaultState);

  const onNicknameChanged = useCallback(
    debounce((value: string) => dispatch({ type: 'nicknameChanged', payload: value }), 500),
    []
  );

  useEffect(() => {
    if (state.nickname === '') {
      if (state.requestStatus === 'idle') {
        return;
      }
      dispatch({ type: 'requestSucceded', payload: [] });
      return;
    }
    if (state.nickname === 'fail') {
      dispatch({ type: 'requestFailed' });
      return;
    }
    (async () => {
      await new Promise((r) => setTimeout(r, 500));
      dispatch({ type: 'requestSucceded', payload: clients.slice(0, 5) });
    })();
  }, [state.nickname]);

  return {
    onNicknameChanged,
    clients: state.result,
    status: state.requestStatus,
  };
};

export default useClientSearch;

type State = {
  requestStatus: 'idle' | 'processing' | 'success' | 'fail';
  nickname: string;
  result: Client.Type[];
};

type Action = {
  type: 'nicknameChanged' | 'requestSucceded' | 'requestFailed' | 'requestProcessing';
  payload?: any;
};

const defaultState: State = {
  requestStatus: 'idle',
  nickname: '',
  result: [],
};

const reducer = (state: State, { type, payload }: Action): State => {
  switch (type) {
    case 'nicknameChanged':
      return {
        ...state,
        nickname: payload,
        requestStatus: 'processing',
        result: [],
      };
    case 'requestFailed':
      return {
        ...state,
        requestStatus: 'fail',
      };
    case 'requestProcessing':
      return {
        ...state,
        requestStatus: 'processing',
      };
    case 'requestSucceded':
      return {
        ...state,
        requestStatus: 'success',
        result: payload,
      };
    default:
      return state;
  }
};
