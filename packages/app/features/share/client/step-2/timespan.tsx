import { Button, XStack } from '@my/ui';
import React from 'react';

const TimespanSelector = ({ selected, onSelectedChange }: TimespanSelectorTypes.Props) => {
  return (
    <XStack spaceDirection="both" flexWrap="wrap" gap="$2">
      {TimespanSelectorTypes.timeSpanMins.map((value, i) => (
        <TimespanSelector.Pill
          key={`TimespanSelector_Pill_${i}`}
          label={TimespanSelectorTypes.timeSpanLabels[i] ?? 'UNKOWN'}
          value={value}
          isSelected={selected === value}
          onPress={onSelectedChange}
        />
      ))}
    </XStack>
  );
};

TimespanSelector.Pill = ({
  label,
  value,
  isSelected = false,
  onPress,
}: TimespanSelectorTypes.Pill.Props) => {
  return (
    <Button
      borderColor="$color8"
      borderStyle="solid"
      borderWidth={isSelected ? '$0.5' : 0}
      bc={isSelected ? '$color5' : undefined}
      size="$3"
      fontSize={16}
      br="$5"
      onPress={() => onPress?.(value)}
    >
      {label}
    </Button>
  );
};

export default TimespanSelector;

export namespace TimespanSelectorTypes {
  export const onceTimeSpan = 0;
  export const foreverTimeSpan = -1;
  export const timeSpanMins = [onceTimeSpan, 60, 1_440, 10_800, 324_000, foreverTimeSpan];
  export const timeSpanLabels = ['jednorázově', '1 hodina', '1 den', '7 dní', '30 dní', 'navždy'];

  export type Props = {
    selected?: number;
    onSelectedChange?: (value: number) => void;
  };

  export namespace Pill {
    export type Props = {
      label: string;
      value: number;
      isSelected?: boolean;

      onPress?: (value: number) => void;
    };
  }
}
