import React, { useState } from 'react';
import { Image, Text } from '@my/ui';
import Layout from '../../layout';
import { useLink } from 'solito/link';

import bg from './assets/bg.png';
import alert from './assets/alert.png';
import TimespanSelector from './timespan';
import { List as ListIcon } from '@tamagui/lucide-icons';
import { useClientShareStore } from 'app/provider/store/share';
import BigInfo from 'app/features/common/biginfo';

const ShareClientStep2Screen = () => {
  const document = useClientShareStore((s) => s.document?.type);
  const setTimeSpan = useClientShareStore((s) => s.setTimespan);
  const [selected, setSelected] = useState<number>();

  const link = useLink({ href: '/share/client/3' });

  const canContinue: boolean = selected !== undefined;

  const onContinuePress = () => {
    setTimeSpan(selected!);
    link.onPress();
  };

  const next = {
    link: { ...link, onPress: onContinuePress },
    isDisabled: !canContinue,
    text: 'Pokračovat na rekapitulaci',
    icon: ListIcon,
  };

  return (
    <>
      <Image source={bg} h="100%" w="100%" pos="absolute" zIndex={0} />
      <Layout document={document} next={next} step="client-2">
        <TimespanSelector selected={selected} onSelectedChange={setSelected} />
        <ShareClientStep2Screen.Alert />
      </Layout>
    </>
  );
};

ShareClientStep2Screen.Alert = () => {
  return (
    <BigInfo text="Volte co nejmenší potřebnou dobu" image={alert} mt="$5" imageSize='$10'>
      <Text flexShrink={1} mt="$-5" fontSize="$4" textAlign="justify">
        Sdílený doklad bude přístupný pouze během zvoleného času.{' '}
        <Text fontWeight="500">Po vypršení doby se adresátu kopie vašeho dokladu zničí.</Text>
      </Text>
    </BigInfo>
  );
};

export default ShareClientStep2Screen;
