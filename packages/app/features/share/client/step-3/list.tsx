import { ListItem, ListItemProps, YGroup } from '@my/ui';
import { File as FileIcon, User as UserIcon, Clock as ClockIcon } from '@tamagui/lucide-icons';
import { ClientShareStore, Store } from 'app/provider/store/share';
import Document from 'app/features/common/documents';
import { TimespanSelectorTypes } from '../step-2/timespan';

const RecapitulationList = ({ store }: RecapitulationListTypes.Props) => {
  if (store.type === 'client') {
    return <RecapitulationList.Client store={store} />;
  } else {
    return <></>;
  }
};

const listItemCommonProps: ListItemProps = {
  bc: '$color3',
  scaleIcon: 1.5,
  textProps: {
    fontWeight: '500',
    fontSize: '$5',
  },
};

RecapitulationList.Client = ({ store }: RecapitulationListTypes.Client.Props) => {
  const documentLabel = store.document && Document.idNameMap[store.document.type];

  const timespanLabel =
    store.timespan !== undefined &&
    TimespanSelectorTypes.timeSpanLabels[
      TimespanSelectorTypes.timeSpanMins.findIndex((t) => t == store.timespan)
    ];

  const targetUserLabel =
    store.targetUser && `@${store.targetUser.nickname} • ${store.targetUser.id}`;

  return (
    <YGroup borderColor="$color6" borderStyle="solid" borderWidth="$1">
      {store.document?.type && (
        <YGroup.Item>
          <ListItem icon={FileIcon} {...listItemCommonProps}>
            {documentLabel}
          </ListItem>
        </YGroup.Item>
      )}
      {store.targetUser && (
        <YGroup.Item>
          <ListItem icon={UserIcon} {...listItemCommonProps}>
            {targetUserLabel}
          </ListItem>
        </YGroup.Item>
      )}
      {store.timespan !== undefined && (
        <YGroup.Item>
          <ListItem icon={ClockIcon} {...listItemCommonProps}>
            {timespanLabel}
          </ListItem>
        </YGroup.Item>
      )}
    </YGroup>
  );
};

namespace RecapitulationListTypes {
  export type Props = {
    store: Store;
  };

  export namespace Client {
    export type Props = {
      store: ClientShareStore;
    };
  }
}

export default RecapitulationList;
