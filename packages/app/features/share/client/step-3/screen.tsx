import React, { useCallback, useState } from 'react';
import { Image, useToastController } from '@my/ui';
import Layout from '../../layout';
import { useClientShareStore } from 'app/provider/store/share';
import { Unlock as UnlockIcon } from '@tamagui/lucide-icons';

import bg from './assets/bg.png';
import RecapitulationList from './list';
import { useRouter } from 'solito/router';
import { useCryptoStore } from 'app/provider/store/crypto';
import { reencryptSymmetricKeyForOther } from 'cryptography/c2c';
import { AsymmetricKey } from 'cryptography/lib/keys';
import { base64ToBytes } from 'cryptography/lib/common/convert';
import { Cryptography } from 'cryptography/lib';

const ShareClientStep3Screen = () => {
  const [loading, setLoading] = useState(false);

  const shareStore = useClientShareStore();
  const encryptedKey = shareStore.document?.encryptedKey;

  const key = useCryptoStore((s) => s.key);

  const toastController = useToastController();

  const { replace } = useRouter();

  const finishFlow = useCallback(() => {
    replace('/');
    toastController.show('Doklad je sdílen úspěšně', { notificationOptions: { icon: 'unlock' } });
  }, []);

  const onContinuePress = () => {
    if (loading) {
      return;
    }

    if (!key || !encryptedKey) {
      return;
    }

    setLoading(true);
    setTimeout(() => {
      const otherKey = AsymmetricKey.getEphemeral();
      const documentEncryptedKey: Cryptography.EncryptedSymmetricKey = {
        value: base64ToBytes(encryptedKey.valueB64),
        nonce: base64ToBytes(encryptedKey.nonceB64),
      };
      reencryptSymmetricKeyForOther(documentEncryptedKey, key, otherKey.publicKey);
      finishFlow();
    }, 0);
  };

  const next = {
    text: 'Povolit přístup uživateli',
    icon: UnlockIcon,
    link: { onPress: onContinuePress },
  };

  return (
    <>
      <Image source={bg} h="100%" w="100%" pos="absolute" zIndex={0} />
      <Layout next={next} step="client-3" isLoading={loading}>
        <RecapitulationList store={shareStore} />
      </Layout>
    </>
  );
};

export default ShareClientStep3Screen;
