import React from 'react';
import CommonHeader from '../common/header';
import Sharing from './constants';
import { IconProps } from '@tamagui/lucide-icons/types/IconProps';
import {
  Clock as ClockIcon,
  List as ListIcon,
  Search as SearchIcon,
  Shuffle as ShuffleIcon,
} from '@tamagui/lucide-icons';
import Document from '../common/documents';

const topic = 'Sdílení';

const icons: { [T in Sharing.Step]?: React.FC<IconProps> } = {
  index: ShuffleIcon,
  'client-1': SearchIcon,
  'client-2': ClockIcon,
  'client-3': ListIcon,
};

const Header = ({ step, document }: HeaderTypes.Props) => {
  const text = Sharing.stepTitleMap[step] ?? 'UNKNOWN';
  const icon = icons[step];
  const subText = document ? Document.idNameMap[document] : undefined;

  const sub = subText ? { topic, text: subText } : undefined;

  return <CommonHeader main={{ text, icon }} sub={sub} />;
};

export default Header;

export namespace HeaderTypes {
  export type Props = {
    step: Sharing.Step;
    document?: Document.Type;
  };
}
