import Sharing from 'app/features/share/constants';
import Document from 'app/features/common/documents';
import { StoreApi, UseBoundStore, create } from 'zustand';

type PersistedDocument = {
  id: string;
  type: Document.Type;
  encryptedKey: {
    valueB64: string;
    nonceB64: string;
  };
};

type PersistedTargetUser = {
  id: string;
  nickname: string;
};

type AbstractShareStore = {
  type: Sharing.Type;
  document?: PersistedDocument;

  setDocument: (document: PersistedDocument | undefined) => void;
  setType: (type: Sharing.Type) => void;
};

export type ClientShareStore = {
  type: 'client';
  targetUser?: PersistedTargetUser;
  timespan?: number;

  setTargetUser: (id: PersistedTargetUser | undefined) => void;
  setTimespan: (timespan: number) => void;
} & AbstractShareStore;

export type OrganizationShareStore = {
  type: 'organization';
} & AbstractShareStore;

export type Store = ClientShareStore | OrganizationShareStore;

export const useShareStore = create<Store>((set) => ({
  type: 'client',
  setDocument: (document) => set({ document }),
  // nasty hack to allow switching store type
  setType: (type: any) => set({ type }),
  setTargetUser: (user) => set({ targetUser: user }),
  setTimespan: (timespan) => set({ timespan }),
}));

export const useClientShareStore = useShareStore as UseBoundStore<StoreApi<ClientShareStore>>;
