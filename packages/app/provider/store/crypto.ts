import { create } from 'zustand';
import { Cryptography } from 'cryptography/lib';
import { AsymmetricKey } from 'cryptography/lib/keys';

type Store = {
  key?: Cryptography.KeyPair;

  setKey: (key: Cryptography.KeyPair) => void;
};

export const useCryptoStore = create<Store>((set) => ({
  key: AsymmetricKey.getEphemeral(),
  setKey: (key) => set({ key }),
}));
