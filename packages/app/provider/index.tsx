import { TamaguiProvider, TamaguiProviderProps, ToastProvider } from '@my/ui';
import { useColorScheme } from 'react-native';

import { ToastViewport } from './ToastViewport';
import config from '../tamagui.config';
import DefaultToast from '@my/ui/src/toasts/DefaultToast';

export function Provider({ children, ...rest }: Omit<TamaguiProviderProps, 'config'>) {
  const scheme = useColorScheme();
  return (
    <TamaguiProvider
      config={config}
      disableInjectCSS
      {...rest}
    >
      <ToastProvider swipeDirection="horizontal" duration={6000}>
        {children}
        <DefaultToast />
        <ToastViewport />
      </ToastProvider>
    </TamaguiProvider>
  );
}
