import { bytesToWords, wordsToBytes } from '../../lib/common/convert';
import CryptoJS from 'crypto-js';

test('bytes to words conversion: empty array', () => {
  const bytes = new Uint8Array();
  expect(bytesToWords(bytes).words).toMatchObject(CryptoJS.lib.WordArray.create().words);
});

test('bytes to words conversion: echo conversion', () => {
  const bytes = new TextEncoder().encode('Hello, world!');
  const words = bytesToWords(bytes);
  expect(wordsToBytes(words)).toMatchObject(bytes);
});

test('words to bytes  conversion: empty array', () => {
  const words = CryptoJS.lib.WordArray.create();
  expect(wordsToBytes(words)).toMatchObject(new Uint8Array());
});

test('words to bytes conversion: echo conversion', () => {
  const words = CryptoJS.lib.WordArray.create([3213123, 4362635, 123, 53465]);
  const bytes = wordsToBytes(words);

  expect(bytesToWords(bytes).words).toMatchObject(words.words);
});
