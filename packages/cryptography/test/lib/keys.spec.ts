import { Cryptography } from '../../lib';
import { Client } from '../../lib/client';
import { wordsToBytes } from '../../lib/common/convert';
import { AsymmetricKey, SymmetricKey } from '../../lib/keys';
import CryptoJS from 'crypto-js';

test('symmetric: length', () => {
  const client: Cryptography.Client = Client.activate('test@test.com', 'HeLLoWorl123D');
  const key = SymmetricKey.generate(client);

  expect(key.words).toHaveLength(16);
});

test('symmetric: encrypt/decrypt cycle', () => {
  const client: Cryptography.Client = Client.activate('test@test.com', 'HeLLoWorl123D');
  const key = SymmetricKey.generate(client);

  const encryptedKey = SymmetricKey.encrypt(key, client.keyPair);
  const decryptedKey = SymmetricKey.decrypt(encryptedKey, client.keyPair);

  expect(decryptedKey?.words).toMatchObject(key.words);
});

test('asymmetric: length', () => {
  const passhash = CryptoJS.SHA256('HeLLoWorl123D');
  const passHashByteArray = wordsToBytes(passhash)
  const key = AsymmetricKey.generate(passHashByteArray);

  expect(key.publicKey).toHaveLength(32);
  expect(key.secretKey).toHaveLength(32);
});
