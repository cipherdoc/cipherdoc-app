import { File } from '../../lib/file';
import CryptoJS from 'crypto-js';
import { SymmetricKey } from '../../lib/keys';
import { Client } from '../../lib/client';
import { wordsToBytes } from '../../lib/common/convert';

test('encrypt/decrypt cycle', () => {
  const file = CryptoJS.lib.WordArray.random(1_000_000); // 2MB
  const fileBytes = wordsToBytes(file);

  const client = Client.activate('testfile@test.file', '79h873t98T9HN8TB6FSAdsa');
  const key = SymmetricKey.generate(client);

  const encryptedFile = File.encrypt(file, key);

  const decryptedFile = File.decrypt(encryptedFile, key);
  const decryptedFileBytes = wordsToBytes(decryptedFile);

  expect(fileBytes).toMatchObject(decryptedFileBytes);
});
