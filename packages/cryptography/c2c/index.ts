import { Cryptography } from '../lib';
import { AsymmetricKey, SymmetricKey } from '../lib/keys';

export const reencryptSymmetricKeyForOther = (
  encryptedSymmetricKey: Cryptography.EncryptedSymmetricKey,
  key: Cryptography.KeyPair,
  otherPublicKey: Cryptography.PublicKey
): Cryptography.EncryptedSymmetricKey | null => {
  const symmetricKey = SymmetricKey.decrypt(encryptedSymmetricKey, key);
  if (!symmetricKey) {
    return null;
  }
  const ephemeralOtherKey = AsymmetricKey.getEphemeral();
  ephemeralOtherKey.publicKey = otherPublicKey;
  return SymmetricKey.encrypt(symmetricKey, ephemeralOtherKey);
};
