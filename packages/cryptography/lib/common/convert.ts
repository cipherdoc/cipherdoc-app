import { Cryptography } from '..';
import CryptoJS from 'crypto-js';

export const wordsToBytes = (words: Cryptography.WordArray): Uint8Array => {
  return wordArrayToByteArray(words.words, words.sigBytes);
};

export const bytesToWords = (bytes: Uint8Array): Cryptography.WordArray => {
  return byteArrayToWordArray(bytes);
};

export const bytesToBase64 = (bytes: Uint8Array): string => Buffer.from(bytes).toString('base64');

export const base64ToBytes = (b64: string): Uint8Array =>
  new Uint8Array(Buffer.from(b64, 'base64'));

const byteArrayToWordArray = (ba: Uint8Array) => {
  let wa: number[] = [],
    i;
  for (i = 0; i < ba.length; i++) {
    wa[(i / 4) | 0] |= ba[i]! << (24 - 8 * i);
  }

  return CryptoJS.lib.WordArray.create(wa, ba.length);
};

const wordToByteArray = (word: number, length: number) => {
  let ba: number[] = [],
    xFF = 0xff;
  if (length > 0) ba.push(word >>> 24);
  if (length > 1) ba.push((word >>> 16) & xFF);
  if (length > 2) ba.push((word >>> 8) & xFF);
  if (length > 3) ba.push(word & xFF);

  return ba;
};

const wordArrayToByteArray = (wordArray: number[], length: number) => {
  let result: number[] = [],
    bytes: number[],
    i = 0;
  while (length > 0) {
    bytes = wordToByteArray(wordArray[i]!, Math.min(4, length));
    length -= bytes.length;
    result.push(...bytes);
    i++;
  }
  return new Uint8Array(result.flat());
};
