import CryptoJS from 'crypto-js';
import { Cryptography } from '..';
import { AsymmetricKey } from '../keys';
import { wordsToBytes } from '../common/convert';

export namespace Client {
  export const activate = (login: string, password: string): Cryptography.Client => {
    const passhash = CryptoJS.SHA256(login + password);
    const passHashByteArray = wordsToBytes(passhash);
    const keyPair = AsymmetricKey.generate(passHashByteArray);

    return {
      keyPair: keyPair,
      passHash: {
        words: passhash,
        bytes: passHashByteArray,
      },
    };
  };
}
