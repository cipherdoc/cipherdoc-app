import nacl from 'tweetnacl';
import CryptoJS from 'crypto-js';
import { Cryptography } from '..';
import { bytesToWords, wordsToBytes } from '../common/convert';

export namespace SymmetricKey {
  export const generate = (client: Cryptography.Client): Cryptography.SymmetricKey => {
    const rand = new TextDecoder().decode(nacl.randomBytes(32));
    const precursor = CryptoJS.SHA256(client.passHash.words.toString() + CryptoJS.SHA256(rand));
    return CryptoJS.EvpKDF(precursor, CryptoJS.lib.WordArray.random(64), {
      keySize: 16,
      iterations: 10_000,
    });
  };

  export const encrypt = (
    value: Cryptography.SymmetricKey,
    key: Cryptography.KeyPair
  ): Cryptography.EncryptedSymmetricKey => {
    const nonce = nacl.randomBytes(24);
    const box = nacl.box(wordsToBytes(value), nonce, key.publicKey, key.secretKey);

    return {
      nonce,
      value: box,
    };
  };

  export const decrypt = (
    { value, nonce }: Cryptography.EncryptedSymmetricKey,
    key: Cryptography.KeyPair
  ): Cryptography.SymmetricKey | null => {
    const bytes = nacl.box.open(value, nonce, key.publicKey, key.secretKey);
    if (!bytes) {
      return null;
    }

    return bytesToWords(bytes);
  };
}

export namespace AsymmetricKey {
  export const generate = (bytes: Uint8Array): Cryptography.KeyPair =>
    nacl.box.keyPair.fromSecretKey(bytes);

  export const getEphemeral = (): Cryptography.KeyPair => nacl.box.keyPair();
}
