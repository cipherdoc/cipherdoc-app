import { Cryptography } from '..';
import CryptoJS from 'crypto-js';

export namespace File {
  export const encrypt = (
    file: Cryptography.File,
    key: Cryptography.SymmetricKey
  ): Cryptography.EncryptedFile => {
    return CryptoJS.AES.encrypt(file, key.toString());
  };

  export const decrypt = (
    encryptedFile: Cryptography.EncryptedFile,
    key: Cryptography.SymmetricKey
  ): Cryptography.File => {
    return CryptoJS.AES.decrypt(encryptedFile, key.toString());
  };
}
