import CryptoJS from 'crypto-js';
import nacl from 'tweetnacl';

export namespace Cryptography {
  export type Client = {
    passHash: PassHash;
    keyPair: KeyPair;
  };

  export type File = WordArray;

  export type EncryptedFile = CryptoJS.lib.CipherParams;

  type PassHash = {
    words: WordArray;
    bytes: Uint8Array;
  };

  export type WordArray = CryptoJS.lib.WordArray;

  export type KeyPair = nacl.BoxKeyPair;

  export type PublicKey = nacl.BoxKeyPair['publicKey'];

  export type SymmetricKey = WordArray;

  export type EncryptedSymmetricKey = {
    value: Uint8Array;
    nonce: Uint8Array;
  };
}
