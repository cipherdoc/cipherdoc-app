import { Theme } from '@my/ui';
import { DefaultTheme, ThemeProvider } from '@react-navigation/native';
import { Provider } from 'app/provider';
import { useFonts } from 'expo-font';
import { Stack } from 'expo-router';

export default function HomeLayout() {
  const [loaded] = useFonts({
    Inter: require('@tamagui/font-inter/otf/Inter-Regular.otf'),
    InterMedium: require('@tamagui/font-inter/otf/Inter-Medium.otf'),
    InterBold: require('@tamagui/font-inter/otf/Inter-Bold.otf'),
  });

  if (!loaded) {
    return null;
  }
  return (
    <Provider>
      <ThemeProvider value={DefaultTheme}>
        <Theme name='teal'>
          <Stack screenOptions={{ headerShown: false }} />
        </Theme>
      </ThemeProvider>
    </Provider>
  );
}
