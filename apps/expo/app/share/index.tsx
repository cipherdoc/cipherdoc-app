import ShareScreen from 'app/features/share/screen';
import { Stack } from 'expo-router';
import React from 'react';

const Screen = () => {
  return (
    <>
      <Stack.Screen
        options={{
          title: 'Share',
        }}
      />
      <ShareScreen />
    </>
  );
};

export default Screen;
