import { Stack } from 'expo-router';
import React from 'react';
import { createParam } from 'solito';

import ShareClientStep1Screen from 'app/features/share/client/step-1/screen';
import ShareClientStep2Screen from 'app/features/share/client/step-2/screen';
import ShareClientStep3Screen from 'app/features/share/client/step-3/screen';


const { useParam } = createParam<{ step: number }>();
const stepParamConfig = {
  initial: 1,
  parse: (value?: string | string[]) => {
    if (typeof value === 'string') {
      const number = parseInt(value);
      if (isNaN(number)) {
        return 0;
      }
      return number;
    }
    return 1;
  },
} as const;

const steps = [ShareClientStep1Screen, ShareClientStep2Screen, ShareClientStep3Screen] as const;

const Screen = () => {
  const [stepNumber] = useParam('step', stepParamConfig);

  const Step = steps[stepNumber - 1];

  if (!Step) {
    return null;
  }

  return (
    <>
      <Stack.Screen
        options={{
          title: 'Share',
        }}
      />
      <Step />
    </>
  );
};

export default Screen;
