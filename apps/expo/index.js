import 'expo-router/entry';
import 'react-native-get-random-values';
global.Buffer = global.Buffer || require('buffer').Buffer;

import { LogBox } from 'react-native';

LogBox.ignoreAllLogs();
